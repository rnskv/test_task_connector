const helpers = {
    getRandomColor: () => {
        const colors = ['#815FE3', '#FCBF23', '#21D29B', '#F86464', '#FC86E2', '#408DD6', '#4ba359', '#4ba359', '#658a9b',  '#D4D4D4'];
        return colors[Math.floor(Math.random() * colors.length)];
    },
    getNodeParams: (node) => {
        const rect = node.getBoundingClientRect()
        return {
            width: node.clientWidth,
            height: node.clientHeight,
            x: rect.left,
            y: rect.top,
        }
    }
}

class Point {
    element = null;

    constructor(element) {
        if (!element) return null;

        this.element = element;
    }

    isValid() {
        return this.element !== null;
    }

    getParams() {
        return helpers.getNodeParams(this.element);
    }
}

class Line {
    root = null;
    target = null;
    timestamp = null;
    color = '#000';
    isRounded = false;
    id = null;

    container = null;
    node = null;
    lineBg = null;
    line = null;
    targetCircleBg = null;
    targetCircle = null;
    rootCircleBg = null;
    rootCircle = null;

    constructor({ root, target, isRounded, container }) {
        this.root = root;
        this.target = target;
        this.timestamp = Date.now();
        this.color = helpers.getRandomColor();
        this.isRounded = isRounded;
        this.id = `f${(~~(Math.random()*1e8)).toString(16)}`;
        this.container = container;

        this.mountNode();
    }

    update() {
        const {
            lineWidth,
            x1,
            y1,
            x2,
            y2
        } = this.getCurrentParams();

        this.lineBg.setAttribute('d', `M${x1} ${y1}, A${this.isRounded ? 1 : 0},${this.isRounded ? 1 : 0} 0 0,1 ${x2},${y2}`);
        this.line.setAttribute('d', `M${x1} ${y1}, A${this.isRounded ? 1 : 0},${this.isRounded ? 1 : 0} 0 0,1 ${x2},${y2}`);
        
        this.targetCircleBg.setAttribute('cx', x2);
        this.targetCircleBg.setAttribute('cy', y2);

        this.targetCircle.setAttribute('cx', x2);
        this.targetCircle.setAttribute('cy', y2);
        
        this.rootCircleBg.setAttribute('cx', x1);
        this.rootCircleBg.setAttribute('cy', y1);

        this.rootCircle.setAttribute('cx', x1);
        this.rootCircle.setAttribute('cy', y1);
    }

    getNode() {
        const node = this.container.getElementById(this.id);
        return node;
    }

    mountNode() {
        const g = document.createElementNS("http://www.w3.org/2000/svg", 'g');

        g.setAttribute('id', this.id);
        g.innerHTML = this.getSvgString();

        this.container.append(g);

        this.node = this.getNode();
        this.lineBg = this.node.querySelector('#line_bg');
        this.line = this.node.querySelector('#line');
        this.targetCircleBg = this.node.querySelector('#targetCircleBg');
        this.targetCircle = this.node.querySelector('#targetCircle');
        this.rootCircleBg = this.node.querySelector('#rootCircleBg');
        this.rootCircle = this.node.querySelector('#rootCircle');
    }

    getCurrentParams() {
        const rootParams = this.root.getParams();
        const targetParams = this.target.getParams();

        const lineWidth = 6;

        const x1 = rootParams.x + rootParams.width / 2 - lineWidth  / 2;
        const y1 = rootParams.y + rootParams.height / 2 - lineWidth / 2;
        const x2 = targetParams.x + targetParams.width / 2 - lineWidth  / 2;
        const y2 = targetParams.y + targetParams.height / 2 - lineWidth / 2;

        return {
            lineWidth,
            x1,
            y1,
            x2,
            y2
        }
    }

    getSvgString() {
        const {
            lineWidth,
            x1,
            y1,
            x2,
            y2
        } = this.getCurrentParams();

        return `
            <path id="line_bg" d="M${x1} ${y1}, A${this.isRounded ? 1 : 0},${this.isRounded ? 1 : 0} 0 0,1 ${x2},${y2}" stroke="black" fill="transparent" style="stroke:#000;
                stroke-width:${lineWidth};
            "/>
            <path id="line" d="M${x1} ${y1}, A${this.isRounded ? 1 : 0},${this.isRounded ? 1 : 0} 0 0,1 ${x2},${y2}" stroke="black" class="path" fill="transparent" style="stroke:${this.color};
                stroke-width:${lineWidth - 2};
            "/>

            <circle id="targetCircleBg" cx="${x2}" cy="${y2}" r="${lineWidth}" fill="black" style="fill:#000"/>
            <circle id="targetCircle" cx="${x2}" cy="${y2}" r="${lineWidth - 2}" fill="black" style="fill:${this.color}"/>

            <circle id="rootCircleBg" cx="${x1}" cy="${y1}" r="${lineWidth}" fill="black" style="fill:#000"/>
            <circle id="rootCircle" cx="${x1}" cy="${y1}" r="${lineWidth - 2}" fill="black" style="fill:${this.color}"/>
        `
    }
}


class Connector {
    lines = [];

    constructor() {
        this.initializeCss();
        this.update();
    }

    setConnection(rootNode, targetNode, isRounded) {
        const rootPoint = new Point(rootNode);
        const targetPoint = new Point(targetNode);
        const container = this.getSvgContainer();

        if (!rootPoint.isValid() || !targetPoint.isValid()) {
            return;
        }

        this.lines.push(
            new Line({
                root: rootPoint,
                target: targetPoint,
                isRounded,
                container,
            })
        )
    }

    update() {
        const container = this.getSvgContainer();

        for (const line of this.lines) {
            line.update();
        }

        requestAnimationFrame(this.update.bind(this))
    }



    connect(rootSelector, targetSelector, isRounded = false, isDeep = false) {
        const rootNodes = document.querySelectorAll(rootSelector);
        const targetNodes = document.querySelectorAll(targetSelector);

        const container = this.getSvgContainer();
        
        if (isDeep) {
            for (const rootNode of rootNodes) {
                for (const targetNode of targetNodes) {
                    this.setConnection(rootNode, targetNode, isRounded)
                }
            }

        } else {
            this.setConnection(rootNodes[0], targetNodes[0], isRounded)
        }
    }

    getSvgContainer() {
        const UID = 'svg_rnsv_container';
        let container = document.querySelector(`#${UID}`);

        if (!container) {
            let { x, y, width, height } = helpers.getNodeParams(document.body);

            width = Math.max(width, window.innerWidth);
            height = Math.max(height, window.innerHeight);

            container = document.createElementNS("http://www.w3.org/2000/svg", "svg");                    
            container.style.position = 'fixed';
            container.style.pointerEvents = 'none';
            container.style.top = 0;
            container.style.left = 0;
            container.style.zIndex = 9999;
            container.setAttribute("id", UID);
            container.setAttribute("width", width + 'px');
            container.setAttribute("height", height + 'px');
            container.setAttribute('viewBox', `0 0 ${width} ${height}`);
            
            document.body.append(container);
        }

        return container;
    }

    initializeCss() {
        const styles = document.createElement('style');

        styles.innerHTML = `
            .path {
                stroke-dasharray: 20;
                stroke-dashoffset: 400;
                animation: dash 5s linear infinite;
            }

            @keyframes dash {
                to {
                    stroke-dashoffset: 0;
                }
            }
        `
        document.body.append(styles);
    }
}


const connector = new Connector();

window.connect = connector.connect.bind(connector);